import React from 'react';
import { hot } from "react-hot-loader";
import { Route, Switch } from "react-router-dom";
import { HomePage } from "./app/pages/Home";
import { UserPage } from "./app/pages/User";
import { Users } from "./app/pages/Users";

const App = hot(module)(() => {
	return (
		<Switch>
			<Route path="/" exact component={ HomePage }/>
			<Route path="/users" exact component={ Users }/>
			<Route path="/users/:id" exact component={ UserPage }/>
		</Switch>
	);
})

export default App;
