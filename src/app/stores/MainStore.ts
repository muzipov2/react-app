import { UserStore } from './UserStore';

class MainStore {
	userStore: UserStore;

	constructor() {
		this.userStore = new UserStore();
	}
}

export const mainStore = new MainStore();

export default mainStore;

export { MainStore };
