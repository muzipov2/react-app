import { action, observable } from 'mobx';
import { RoleEnum, UserModel } from '../models/User.model';


export class UserStore {
	@observable users: Array<UserModel> = [
		{
			name: 'Пользователь 1',
			role: RoleEnum.User,
			isActive: true,
			id: 1,
		},
		{
			name: 'Пользователь 2',
			role: RoleEnum.Admin,
			isActive: true,
			id: 2,
		},
		{
			name: 'Пользователь 3',
			role: RoleEnum.Moderator,
			isActive: true,
			id: 3,
		},
		{
			name: 'Пользователь 4',
			role: RoleEnum.User,
			isActive: false,
			id: 4,
		}
	];

	@action
	addUser = (user: UserModel) => {
		this.users.push(user);
	}

	@action
	removeUser = (userId: number) => {
		const userIndex = this.users.findIndex((user: UserModel) => user.id === userId);
		this.users.splice(userIndex, 1);
	}
}

