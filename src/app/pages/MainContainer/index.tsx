import { FC, ReactNode } from "react";
import { NavLink } from 'react-router-dom';
import styles from './index.module.sass';

interface MainContainerProps {
	children: ReactNode;
}

export const MainContainer: FC<MainContainerProps> = ({children}) => {
	return (
		<div className={ styles.container }>
			<header className={ styles.header }>
				<span className={styles.logo}>Logo</span>

				<nav className={ styles.headerNav }>
					<NavLink to='/' exact className={styles.link} activeClassName={ styles.linkActive }>
						Главная
					</NavLink>
					<NavLink to='/users' className={styles.link} activeClassName={ styles.linkActive }>
						Пользователи
					</NavLink>
				</nav>
			</header>

			<main>
				{children}
			</main>
		</div>
	)
}
