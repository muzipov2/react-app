import cn from "classnames/bind";
import { FC } from "react";
import { NavLink } from "react-router-dom";
import { UserModel } from "../../../models/User.model";
import { formatUserRole } from "../../../utils";
import styles from './index.module.sass';


const cx = cn.bind(styles);

interface UserProps {
	user: UserModel;
	onDelete: (id: number) => void;
}

export const User: FC<UserProps> = ({ user, onDelete }) => {
	return (
		<div className={ styles.user }>
			<div className={ cx(styles.userStatus, {
				userStatusActive: user.isActive,
			}) }/>
			<div className={ styles.userName }>{ user.name }</div>
			<div className={ styles.userRole }>Роль: { formatUserRole(user.role) }</div>
			<NavLink to={`/users/${user.id}`}>Перейти</NavLink>

			<button onClick={() => onDelete(user.id)}>X</button>
		</div>
	)
}
